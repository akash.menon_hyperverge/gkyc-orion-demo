export const getFaceConfig = (email) => {
  const hvFaceConfig = new window.HVFaceConfig();

  hvFaceConfig.setShouldShowInstructionPage(true);
  hvFaceConfig.setLivenessAPIParameters({ qualityCheck: 'yes' });
  hvFaceConfig.setLivenessAPIHeaders({ email });
  hvFaceConfig.setShouldHandleRetries(true);

  return hvFaceConfig;
};

const startFaceCapture = (email) => {
  const hvFaceConfig = getFaceConfig(email);

  return new Promise((resolve, reject) => {
    window.HVFaceModule.start(hvFaceConfig, (HVError, HVResponse) => {
      if (HVError) {
        const errorCode = HVError.getErrorCode();
        const errorMessage = HVError.getErrorMessage();
        return reject({ errorCode, errorMessage });
      }

      const apiResults = HVResponse.getApiResult();
      const imageBase64 = HVResponse.getImageBase64();
      const attemptsCount = HVResponse.getAttemptsCount();
      return resolve({ apiResults, imageBase64, attemptsCount });
    });
  });
};

export default startFaceCapture;
