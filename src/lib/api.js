import axios from 'axios';
import { getAuth } from 'firebase/auth';
import app from '../firebase';
import { ORION_FACEAUTH, ORION_VERIFY } from './constants';

const BASE_URL =
  window.location.hostname.includes('localhost') ||
  window.location.hostname.includes('127.0.0.1')
    ? process.env.REACT_APP_BASE_DEV_URL
    : process.env.REACT_APP_BASE_PROD_URL;

console.log(`Base url is ${BASE_URL}`);

const auth = getAuth(app);
let idToken = null;

const getIdToken = async () => {
  if (idToken) return idToken;
  idToken = await auth.currentUser.getIdToken();
  return idToken;
};

const config = { baseURL: BASE_URL };

export const getJWTToken = async (useCase) => {
  const idtoken = await getIdToken();
  return axios.get('/token', {
    ...config,
    headers: { idtoken },
    params: { useCase },
  });
};

const convertBase64ToFile = async (base64, fileName) => {
  const res = await fetch(base64);
  const blob = await res.blob();
  const file = new File([blob], fileName, { type: 'image/png' });

  return file;
};

export const makeGenericOrionCall = async (selfie, transactionId, useCase) => {
  const formData = new FormData();
  const selfieImage = await convertBase64ToFile(selfie, 'selfie');

  formData.append('selfie', selfieImage);
  formData.append('transactionId', transactionId);

  const idtoken = await getIdToken();

  let endpoint = '';

  if (useCase === ORION_VERIFY) endpoint = '/orion/generic/verify';
  else if (useCase === ORION_FACEAUTH) endpoint = '/orion/generic/faceauth';

  return axios.post(endpoint, formData, {
    ...config,
    headers: { 'Content-Type': 'multipart/form-data', idtoken },
  });
};
