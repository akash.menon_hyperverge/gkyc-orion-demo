export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

export const KYC_USECASE = 'KYC';
export const ORION_USECASE = 'ORION';

export const ORION_VERIFY = 'VERIFY';
export const ORION_FACEAUTH = 'FACEAUTH';

export const SUCCESS_COLOR = '#159D00';
export const FAIL_COLOR = '#FA0015';
export const MANUAL_COLOR = '#D19A00';
