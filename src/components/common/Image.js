/* eslint-disable no-bitwise */
import React from 'react';
import styled from 'styled-components';
import { Col } from 'rsuite';
import PropTypes from 'prop-types';

const Title = styled.h1`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  height: 40px;
  padding: 0 16px;
  font-size: 30px;
  font-weight: 500;
`;

export const ImageContainer = styled.div`
  width: 100%;
  max-width: 340px;
  height: 260px;
  border-radius: 5px;
  background-color: #edeeef;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  margin: 0 auto 30px;
  padding: 0 10px;
  margin-top: 5%;
`;

export const Image = styled.img`
  max-height: 100%;
  max-width: 100%;
`;

const ImageCaption = styled.span`
  position: absolute;
  width: 100%;
  text-align: center;
  bottom: -15%;
  margin-top: 15%;
  font-size: 20px;
`;

const altImage =
  'https://roofequipmentllc.com/wp-content/uploads/2019/01/noimage.png';

const Images = ({ images }) => {
  // const width = 24 / images.length;
  return (
    <>
      <Title>Captured Images</Title>
      {images.map((imageObj, idx) => (
        <Col sm={8} smOffset={idx === 0 ? 3 : 0} xs={20} xsOffset={2} key={idx}>
          <ImageContainer key={idx}>
            <Image src={imageObj.image || altImage} alt={imageObj.title} />
            <ImageCaption>{imageObj.title}</ImageCaption>
          </ImageContainer>
        </Col>
      ))}
    </>
  );
};

Images.propTypes = {
  images: PropTypes.instanceOf(Array).isRequired,
};

export default Images;
