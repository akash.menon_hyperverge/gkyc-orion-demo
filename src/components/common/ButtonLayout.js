import React from 'react';
import { Row, Col } from 'rsuite';
import PropTypes from 'prop-types';

const ButtonLayout = ({ children }) => {
  return (
    <Row style={{ marginTop: '5%' }}>
      <Col xs={20} xsOffset={2} md={10} mdOffset={7}>
        {children}
      </Col>
    </Row>
  );
};

ButtonLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ButtonLayout;
