import React from 'react';
import { Grid, Row, Col, Loader } from 'rsuite';
import PropTypes from 'prop-types';
import Logo from './Logo';
import Layout from '../common/Layout';
import KycContainer from '../../containers/Kyc';
import OrionContainer from '../../containers/Orion';

const HomeScreen = ({ loading, jwtToken }) => {
  const content = !loading ? (
    <>
      <Grid fluid style={{ marginTop: '5%' }}>
        <Row style={{ textAlign: 'center' }}>
          <Col md={12} mdOffset={6} xs={20} xsOffset={2}>
            <Logo />
          </Col>
        </Row>
        <OrionContainer />
        <KycContainer jwtToken={jwtToken} />
      </Grid>
    </>
  ) : (
    <Loader center size="lg" content="Loading..." />
  );

  return <Layout content={content} />;
};

HomeScreen.propTypes = {
  loading: PropTypes.bool.isRequired,
  jwtToken: PropTypes.string.isRequired,
};

export default HomeScreen;
