import React, { useState } from 'react';
import { Modal, Form, Grid, Row, Col, Button } from 'rsuite';
import PropTypes from 'prop-types';
import { Image, ImageContainer } from '../common/Image';
import { FAIL_COLOR } from '../../lib/constants';

const InputResultModal = ({
  isOpen,
  onHide,
  setUserInput,
  onSubmit,
  formData,
  selfie,
  buttonLoading,
  result,
  qualityIssue,
  retry,
}) => {
  const initialErrorState = {
    transactionId: false,
  };
  const [formErrors, setFormErrors] = useState(initialErrorState);

  const onChange = (e) => {
    const transactionId = e.transactionId ?? formData.transactionId;

    setUserInput({ transactionId });
  };

  const submitForm = () => {
    const errors = {
      transactionId: false,
    };
    if (!formData.transactionId.length) errors.transactionId = true;

    if (errors.transactionId) setFormErrors(errors);
    else onSubmit();
  };

  const getErrorMsg = (key) => {
    if (formErrors[key]) return 'Required Field';
    return null;
  };

  const transactionIdErrorMsg = getErrorMsg('transactionId');
  const buttonText = qualityIssue.issuePresent ? 'Retry' : 'Submit';
  const buttonAction = qualityIssue.issuePresent ? retry : submitForm;

  const bodyContent = !qualityIssue.issuePresent ? (
    <>
      <Form onChange={onChange} fluid>
        <Row>
          <Col xs={22} xsOffset={1} md={20} mdOffset={2}>
            <Form.Group>
              <Form.ControlLabel>
                <span style={{ fontSize: '15px', fontWeight: 'bold' }}>
                  Enter Unique ID
                </span>
              </Form.ControlLabel>
              <Form.Control
                name="transactionId"
                value={formData.transactionId}
                errorPlacement="bottomEnd"
                errorMessage={transactionIdErrorMsg}
                placeholder="Mobile Number"
              />
              <Form.HelpText tooltip>Required</Form.HelpText>
            </Form.Group>
          </Col>
        </Row>
      </Form>
      <Row
        style={{
          textAlign: 'center',
        }}
      >
        {result}
      </Row>
    </>
  ) : (
    <Row
      style={{
        textAlign: 'center',
      }}
    >
      <span style={{ color: FAIL_COLOR }} className="resultText">
        {`Please try again, ${qualityIssue.issue.message}`}
      </span>
    </Row>
  );

  return (
    <Modal
      open={isOpen}
      onClose={() => {
        onHide(false);
        setFormErrors(initialErrorState);
      }}
      size="lg"
      style={{ maxWidth: '100%' }}
      backdrop="static"
    >
      <Modal.Header />
      <Modal.Body>
        <ImageContainer>
          <Image src={selfie} alt="selfie" />
        </ImageContainer>
        <Grid style={{ width: '90%' }}>{bodyContent}</Grid>
      </Modal.Body>
      <Modal.Footer
        style={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Button
          onClick={buttonAction}
          appearance="primary"
          size="md"
          loading={buttonLoading}
          className="button"
          block
          style={{
            maxWidth: '35%',
          }}
        >
          {buttonText}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

InputResultModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired,
  setUserInput: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  formData: PropTypes.instanceOf(Object).isRequired,
  selfie: PropTypes.string,
  buttonLoading: PropTypes.bool.isRequired,
  result: PropTypes.node.isRequired,
  qualityIssue: PropTypes.instanceOf(Object).isRequired,
  retry: PropTypes.func.isRequired,
};

InputResultModal.defaultProps = {
  selfie: '',
};

export default InputResultModal;
