import React, { useEffect, useRef, useState } from 'react';
import { getJWTToken } from '../../lib/api';
import HomeScreen from '../../components/Home/Screen';
import { KYC_USECASE } from '../../lib/constants';

const HomeContainer = () => {
  const [loading, setLoading] = useState(true);
  const [jwtToken, setJwtToken] = useState('');
  const getJWTTokenApiRef = useRef(getJWTToken);

  useEffect(() => {
    const initializeSdk = async () => {
      const jwtToken = (await getJWTTokenApiRef.current(KYC_USECASE)).data
        .token;
      setJwtToken(jwtToken);
      setLoading(false);
    };
    initializeSdk();
  }, []);

  return <HomeScreen loading={loading} jwtToken={jwtToken} />;
};

export default HomeContainer;
