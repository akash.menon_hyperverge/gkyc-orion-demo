/* eslint-disable no-undef */
import React, { useState } from 'react';
import { Button, toaster, Message, Row } from 'rsuite';
import PropTypes from 'prop-types';
import ButtonLayout from '../../components/common/ButtonLayout';
import getRandomString from '../../lib/randomString';
import { SUCCESS_COLOR, FAIL_COLOR, MANUAL_COLOR } from '../../lib/constants';
import Images from '../../components/common/Image';

const KycContainer = ({ jwtToken }) => {
  const [kycResult, setKycResult] = useState(null);

  const onResult = (HyperKycResult) => {
    setKycResult(HyperKycResult);

    if (kycResult?.Cancelled) {
      toaster.push(
        <Message type="error" showIcon className="alert">
          KYC Cancelled
        </Message>,
        {
          placement: 'topCenter',
        }
      );
    } else if (kycResult?.Failure) {
      toaster.push(
        <Message type="error" showIcon className="alert">
          KYC Failed
        </Message>,
        {
          placement: 'topCenter',
        }
      );
    }
  };

  const images = [];
  const texts = [];
  let resultElement;

  const getStatusFromAction = (action) => {
    if (action === 'pass') return SUCCESS_COLOR;
    if (action === 'fail') return FAIL_COLOR;
    return MANUAL_COLOR;
  };

  if (kycResult?.Success) {
    const { docListData, faceData, faceMatchData } = kycResult?.Success?.data;

    docListData?.forEach((doc) => {
      const { action, docImagePath, side } = doc;

      const message = `Document ${
        side[0] + side.slice(1).toLowerCase()
      } Result - ${action[0].toUpperCase() + action.slice(1)}`;
      const status = getStatusFromAction(action);

      images.push({
        image: docImagePath,
        title: `Document ${side[0] + side.slice(1).toLowerCase()}`,
      });
      texts.push({
        message,
        status,
      });
    });

    if (faceData) {
      const { action, fullFaceImagePath } = faceData;
      const status = getStatusFromAction(action);
      const message = `Selfie Result - ${
        action[0].toUpperCase() + action.slice(1)
      }`;

      images.push({ image: fullFaceImagePath, title: 'Selfie' });
      texts.push({
        message,
        status,
      });
    }

    if (faceMatchData) {
      const { action } = faceMatchData;
      const status = getStatusFromAction(action);
      const message = `FaceMatch Result - ${
        action[0].toUpperCase() + action.slice(1)
      }`;

      texts.push({ message, status });
    }

    const textElements = texts.map(({ message, status }, idx) => {
      return (
        <Row style={{ textAlign: 'center', margin: '1% 0' }} key={idx}>
          <span style={{ color: status }} className="resultText">
            {message}
          </span>
        </Row>
      );
    });

    resultElement = (
      <>
        <Row
          style={{
            marginTop: '5%',
            textAlign: 'center',
          }}
        >
          <Images images={images} />
        </Row>
        {textElements}
      </>
    );
  }

  const startKyc = () => {
    const transactionId = getRandomString(12);

    const document = new Document(true, '', '');
    const face = new Face();

    const workflow = [face, document];
    const hyperKycConfig = new window.HyperKycConfig(
      jwtToken,
      workflow,
      transactionId,
      'twn'
    );

    window.HyperKYCModule.launch(hyperKycConfig, onResult);
  };

  return (
    <>
      <ButtonLayout>
        <Button
          appearance="primary"
          block
          onClick={startKyc}
          className="button"
        >
          Start KYC
        </Button>
      </ButtonLayout>
      {resultElement}
    </>
  );
};

KycContainer.propTypes = {
  jwtToken: PropTypes.string.isRequired,
};

export default KycContainer;
