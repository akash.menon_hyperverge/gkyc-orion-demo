import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  signInWithEmailAndPassword,
  getAuth,
  onAuthStateChanged,
} from 'firebase/auth';
import { Loader } from 'rsuite';
import LoginScreen from '../../components/Login/Screen';
import app from '../../firebase';
import { setUser } from '../../actions/authActions';
import { LOGIN } from '../../lib/constants';

const LoginContainer = ({ setUser }) => {
  const [error, setError] = useState({ error: false });
  const [loading, setLoading] = useState(true);

  const auth = getAuth(app);

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      setLoading(false);
      if (user) setUser(user.email, LOGIN);
    });
  }, []);

  const onSubmit = async (email, password) => {
    try {
      const response = await signInWithEmailAndPassword(auth, email, password);
      setUser(response.user.email, LOGIN);
    } catch (error) {
      setError({ error: true });
    }
  };

  if (loading) return <Loader backdrop center size="lg" content="Loading..." />;
  return <LoginScreen onSubmit={onSubmit} error={error} />;
};

LoginContainer.propTypes = {
  setUser: PropTypes.func.isRequired,
};

const mapDispatchToProps = { setUser };

export default connect(null, mapDispatchToProps)(LoginContainer);
