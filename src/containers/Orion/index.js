import React, { useState, useEffect, useRef } from 'react';
import { Button } from 'rsuite';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ButtonLayout from '../../components/common/ButtonLayout';
import InputResultModal from '../../components/Orion/InputResultModal';
import { getJWTToken, makeGenericOrionCall } from '../../lib/api';
import startFaceCapture from '../../lib/hypersnap/selfie';
import {
  FAIL_COLOR,
  ORION_FACEAUTH,
  ORION_USECASE,
  ORION_VERIFY,
  SUCCESS_COLOR,
} from '../../lib/constants';
import getRandomString from '../../lib/randomString';

const OrionContainer = ({ user }) => {
  const [userInput, setUserInput] = useState({
    transactionId: '',
  });
  const [showForm, setShowForm] = useState(false);
  const [selfieResult, setSelfieResult] = useState(null);
  const [orionResult, setOrionResult] = useState(null);
  const [loading, setLoading] = useState(false);
  const [selfieQualityIssue, setSelfieQualityIssue] = useState({
    issuePresent: false,
    issue: null,
  });
  const [useCase, setUseCase] = useState('');

  const getJWTTokenApiRef = useRef(getJWTToken);

  useEffect(() => {
    const initializeSdk = async () => {
      const { token } = (await getJWTTokenApiRef.current(ORION_USECASE)).data;
      window.HyperSnapSDK.init(
        token,
        window.HyperSnapParams.Region.AsiaPacific
      );

      const transactionId = getRandomString(12);
      window.HyperSnapSDK.startUserSession(transactionId);
    };
    initializeSdk();
  }, []);

  const startOrion = async (selectedUseCase) => {
    setUseCase(selectedUseCase);
    setSelfieResult(null);
    setOrionResult(null);
    setSelfieQualityIssue({
      issuePresent: false,
      issue: null,
    });
    setShowForm(false);
    setUserInput({
      transactionId: '',
    });
    try {
      const selfieResult = await startFaceCapture(user);
      const { apiResults } = selfieResult;

      if (apiResults?.result?.summary?.action === 'pass') {
        setUserInput({ transactionId: '' });
        setSelfieResult(selfieResult);
        setShowForm(true);
      } else if (apiResults?.result?.summary?.action === 'manualReview') {
        setSelfieQualityIssue({
          issuePresent: true,
          issue: apiResults?.result?.summary?.details?.[0],
        });
        setSelfieResult(selfieResult);
        setShowForm(true);
      } else console.log(`${selectedUseCase} failed`);
    } catch (error) {
      console.log(error);
    }
  };

  const onFormsubmit = async () => {
    setOrionResult(null);

    const { imageBase64 } = selfieResult;

    setLoading(true);

    const orionResponse = await makeGenericOrionCall(
      imageBase64,
      userInput.transactionId,
      useCase
    ).catch((err) => console.log(err));

    setLoading(false);
    setOrionResult({ apiResult: orionResponse.data, useCase });
  };

  const getOrionResult = () => {
    if (!orionResult) return null;
    const { apiResult, useCase } = orionResult;

    let message;
    let status;

    if (useCase === ORION_VERIFY) {
      const { value, reason } = apiResult.result.data.isEnrolled;

      if (value === 'yes') {
        message = 'Application Enrolled Successfully';
        status = SUCCESS_COLOR;
      } else {
        message = `Application Not Enrolled. ${reason}`;
        status = FAIL_COLOR;
      }
    } else {
      const { action } = apiResult.result.summary;

      if (action === 'pass') {
        message = 'Authentication Successful';
        status = SUCCESS_COLOR;
      } else {
        message = 'Authentication Failed';
        status = FAIL_COLOR;
      }
    }

    return (
      <span style={{ color: status }} className="resultText">
        {message}
      </span>
    );
  };

  const orionResultElement = getOrionResult();

  return (
    <>
      <ButtonLayout>
        <Button
          appearance="primary"
          block
          onClick={() => {
            startOrion(ORION_VERIFY);
          }}
          className="button"
        >
          Register User
        </Button>
      </ButtonLayout>
      <ButtonLayout>
        <Button
          appearance="primary"
          block
          onClick={() => {
            startOrion(ORION_FACEAUTH);
          }}
          className="button"
        >
          Verify User
        </Button>
      </ButtonLayout>
      <InputResultModal
        isOpen={showForm}
        onHide={setShowForm}
        setUserInput={setUserInput}
        formData={userInput}
        onSubmit={onFormsubmit}
        selfie={selfieResult?.imageBase64}
        buttonLoading={loading}
        result={orionResultElement}
        qualityIssue={selfieQualityIssue}
        retry={() => startOrion(useCase)}
      />
    </>
  );
};

OrionContainer.propTypes = {
  user: PropTypes.string.isRequired,
};

const mapStateToProps = ({ user }) => ({
  user,
});

export default connect(mapStateToProps)(OrionContainer);
